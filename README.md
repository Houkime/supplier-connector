A few scripts that make use of https://clearspending.ru/opendata/ to find probable cartel candidates.  

Right now they can:

* Fetch contracts database download links from ClearSpending
* Fetch files from these links (~20Gb)
* Compile a preliminary whitelist of inns based on whether they have competed at all (around 2 hrs and 5.5 Gb breathspace required)
* Try to clusterize TINs based on suppliers data (can be downloaded from the same place as contracts)
* The resulting clusters may be fed to https://gitlab.com/Houkime/state-purchases-table-conversion scripts for more in-depth analysis and bureaucracy finishing.
* Profit! (though statistics of successful hits were not yet obtained. Testing is needed).
