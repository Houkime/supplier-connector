#! /bin/env python3

import simplejson as json
import sqlite3
import re
import sys

keys = []
whitelist_file = ''

MIN_SUM=10000000

def prepare_address(a):
    res = a.upper()
    res=res.replace("Г.","Г ")
    res=res.replace("ДОМ","Д.")
    res=res.replace("ОФИС","ОФ.")
    res=res.replace("СТРОЕНИЕ","СТР.")
    res=res.replace("ПЕРЕУЛОК","ПЕР.")
    res=res.replace("ПЕР ","ПЕР.")
    res=res.replace("Д.","")
    res=res.replace("  "," ")
    res=res.replace(","," ")
    res=res.replace("  "," ")
    res=res.replace("КОМНАТА","")
    res=res.replace("КОМН.","")
    res=res.replace("КОМН","")
    res=res.replace(". ",".")
    res=res.replace("  "," ")
    res=res.replace("МОСКВА 77","МОСКВА")
    return res

def calc_sum(entry):
    k = entry.keys()
    moneysum = 0
    if 'contracts223Sum' in k :
        moneysum+=float(entry['contracts223Sum'])
    if 'contractsSum' in k :
        moneysum+=float(entry['contractsSum'])
    return moneysum

def isvalid(entry):
    
    return (calc_sum(entry) >= MIN_SUM)

def has_cyrillic(text):
    return bool(re.search('[\u0400-\u04FF]', text))

def validate_email(mail):
    if not "@" in mail:
        return False
    if "*" in mail or "/" in mail:
        return False
    if has_cyrillic(mail):
        return False
    if len(mail)<5:
        return False
    
    suffixes = mail.split("@")
    if len(suffixes) != 2:
        return False
    if not "." in suffixes[1]:
        return False
    if len (suffixes[0]) < 2:
        return False
    if len (set(suffixes[0])) < 2:
        return False
    if suffixes[0].isnumeric():
        if suffixes[0] in ['123','1234','123456','1234567']:
            return False
#        if suffixes[0] == "".join([str(i) for i in range(len(suffixes[0]))]):
#            return False
    
    return True
    #if suffixes[]

def printProgress (iteration, suffix = 'lines scanned'):
   
    print('\r %s: %d ' % (suffix, iteration), end = '\r')

def analyze_line(line,con):
    #global keys
    line = line[:-2]
    #print (line)
    entry = json.loads(line)
    k=entry.keys()
    if 'inn' in k:
        if isvalid(entry):
            moneysum = calc_sum(entry)
            if 'contactEMail' in k :
                con.execute("INSERT INTO badguys VALUES (?,?,?)",(entry['inn'],entry['contactEMail'],moneysum))
            if 'factualAddress' in k:
                con.execute("INSERT INTO badguys_address VALUES (?,?,?)",(entry['inn'],prepare_address(entry['factualAddress']),moneysum)) 

def load_whitelist(path,con):
    con.execute('''CREATE TABLE allowed
            (inn text)''')
    f = open(path)
    i=0
    while True:
        line = f.readline().rstrip('\n')
        if not len(line) == 0:
            con.execute("INSERT INTO allowed VALUES (?)",(line,))
            printProgress(i)
            i+=1
        else:
            return

target_file = ''
output_file = ''

index = 1

while index < len(sys.argv):
    arg = sys.argv[index]
    index += 1
    if arg in ['--whitelist']:
        arg = sys.argv[index]
        whitelist_file=arg
        index += 1
    elif target_file == '':
        target_file = arg
    elif output_file == '':
        output_file = arg
    

f=open(target_file)
i=0
con = sqlite3.connect(":memory:")
con.execute('''CREATE TABLE badguys
            (inn text, email text, moneysum float)''')
con.execute('''CREATE TABLE badguys_address
            (inn text, address text, moneysum float)''')

if whitelist_file!='':
    print('reading whitelist')
    load_whitelist(whitelist_file,con)
    print ('whitelist read')

analyze_line(f.readline()[1:],con)

while True:

    line = f.readline()
    if len(line) == 0:
        break
#    if i > 6:
#        break
    analyze_line(line,con)
    printProgress(i)
    i+=1

print ("suppliers are scanned")
j=0
for row in con.execute("select inn, email from badguys"):
    j+=1
    #print(row)
print ("with emails: ", j)

j=0
for row in con.execute("select inn, address from badguys_address"):
    j+=1
print ("with address: ", j)
#for row in con.execute('''SELECT inn, COUNT(email) 
#                        FROM badguys
#                        GROUP BY email
#                        HAVING COUNT(email) > 1'''):
#    print(row) 

con.execute('''
                                CREATE VIEW addressguys AS
                                SELECT inn, address
                                FROM badguys_address
                                GROUP BY address
                                HAVING COUNT(address) > 2''')

addressgroups = []
k=0
mass_addresses = len(list(con.execute('''SELECT DISTINCT address FROM addressguys''')))
print ("number of distinct adresses in dupes", mass_addresses )

#for a in con.execute('''SELECT DISTINCT address FROM addressguys'''):
#    addressgroup=list(con.execute("SELECT inn FROM badguys_address WHERE address=?",a))
#    addressgroups.append([x[0] for x in addressgroup])
#    k+=1
#    print (k, ", ".join(addressgroups[-1]+[a[0]]))

mailgroups = []
k=0
for mail in con.execute('''
                                SELECT email, group_concat(inn,";"), sum(moneysum) 
                                FROM badguys %s
                                GROUP BY email
                                HAVING COUNT(email) > 2'''%('NATURAL JOIN allowed' if whitelist_file else '')):
    
    money=mail[2]
    inns = mail[1].split(';')
    inns = list(set(inns))
    if money>= 1000000000 and len(inns) < 30 and len(inns) > 2 and validate_email(mail[0]):
        mailgroups.append(inns)
        mailgroups[-1].append(mail[0])
        mailgroups[-1].append(str(money))
    k+=1
    
print ("number of groups with matching emails: ", len(mailgroups))

mailgroups = sorted(mailgroups, key = lambda x: float(x[-1]))

for m in mailgroups:
    print (mailgroups.index(m), ", ".join(m))
    print ('')
    

#print (keys)
f.close()
con.close()