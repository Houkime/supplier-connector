#! /bin/env python3

import simplejson as json
import sqlite3
import sys
import os
import glob
import zipfile

keys = {}
supplier_keys = {}
supplierstat = {}
nowaycode = 0

def printProgress (iteration, suffix = 'lines scanned'):
   
    print('\r %s: %d ' % (suffix, iteration), end = '\r')

def keystat(entry,dic):
    for k in entry.keys():
       if k not in dic:
            dic[k]=0
    for k in entry.keys():
        dic[k]+=1

def print_key(entry, keyz):
    for key in keyz:
        if key in entry.keys():
            print(entry[key])
            
def validate_placing(code):
    if 'EAP' in code or 'PK' in code:
        return True
    return False

def add_inn(inn,con):
    con.execute("INSERT INTO inns VALUES (?)",(inn,))

def extract_inn(entry,con):
    if "suppliers" in entry.keys():
        for s in entry["suppliers"]:
            if 'inn' in s.keys():
                add_inn(s['inn'],con)

def analyze_line(line,con):
    #global keys
    global nowaycode
    line = line[:-2]
    #print (line)
    entry = json.loads(line)
    keystat(entry,keys)
    k = entry.keys()
    if "singleCustomerReason" in k:
        return
    if 'placingWayCode' in k:
        if not validate_placing(entry['placingWayCode']):
            return
    if 'purchaseInfo' in k:
        pi=entry['purchaseInfo']
        if "purchaseMethodCode" in pi.keys():
            if float(pi["purchaseMethodCode"]) in [40000, 20189]:
                return
    if 'type' in k:
        if entry['type']=='N':
            return
                
    
    extract_inn(entry,con)
            

def load_file(path,con):
    print ('opening', path)
    f=open(path)
    analyze_line(f.readline()[1:],con) #simplejson actually can just load files BUT it is REALLY heavy on memory and doesn't worth it
    
    i=0
    while True:
    
        line = f.readline()
        if len(line) == 0:
            break
        analyze_line(line,con)
        printProgress(i)
        i+=1
    f.close()
       

target_file = ''
output_file = ''

index = 1
while index < len(sys.argv):
    arg = sys.argv[index]
    index += 1
    if target_file == '':
        target_file = arg
    elif output_file == '':
        output_file = arg
    

print ('target is:', target_file)
print ('out is :', output_file)

con = sqlite3.connect(":memory:")
con.execute('''CREATE TABLE inns
            (inn text)''')

    

files = sorted(glob.glob(target_file))
filecount=len(files)
print(files)
for file in files:
    print ('[',files.index(file),'/',filecount,']')
    if '.zip' in file:
        print('unzipping', file)
        zip_ref = zipfile.ZipFile(file, 'r')
        jsons=[x.filename for x in zip_ref.filelist if '.json' in x.filename]
        for j in jsons:
            zip_ref.extract(j,'.')
            load_file('./'+j,con)
            os.remove('./'+j)
        zip_ref.close()
        

print (keys)
#print ("supplierstats: ", supplierstat)
#print ('without pcode: ', nowaycode)

i=0
for inn in con.execute("SELECT inn FROM inns"):
    i+=1

#print ('number of nondistinct inns: ', i)

print ("writing inns to " + output_file)
out=open(output_file, "w")
i=0
for inn in con.execute("SELECT DISTINCT inn FROM inns"):
    out.write(inn[0]+'\n')
    i+=1

print ("completed with ", i, " unique inns")
#print ('number of distinct inns: ', i)

out.close()

con.close()