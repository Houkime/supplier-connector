#! /bin/env python3

from bs4 import BeautifulSoup
import requests

USER_AGENT = 'Anticorruption Cartel Checker Bot'
COMMON = 'https://clearspending.ru'
OPENDATA = 'https://clearspending.ru/opendata/'

def load_url(url):
    try:
        #print ('loading url: '+url)
        headers = {'User-Agent': USER_AGENT}
        res = requests.get(url, headers=headers, timeout=60)
        res.raise_for_status()
        return res.text

    except Exception as e:
        print(url+'not responding')
        print(e)
        
text=load_url(OPENDATA)
soup = BeautifulSoup(text,'html.parser')
l = list(soup.find(attrs={"id":"contracts-list"}).find_all('a'))
links= [x.get("href") for x in l]
links= [COMMON + x for x in links if '#' not in x]

for l in links: print(l)